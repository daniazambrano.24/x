INSERT INTO detail_user(id, first_name, last_name, document_number, gender) VALUES (1, 'dania', 'zambrano','12345','MALE'), (2,'lina', 'millan', '1234', 'MALE');
INSERT INTO users(id, email,enabled, password, user_name, id_detail_user) VALUES (1,'daniazambrano.24@gmail.com', true,'$2a$10$nzx2AgD5kONmzDnGjOpBe.2fBSlZVR616D64zoeebvAF0SoY9FeZa', 'dania',1),(2,'linamillan@gmail.com', true,'$2a$10$SxZtfexebIiyCFl/BAvPm.I.OE9IGWbNKHvx2i4qtYekedVnOLrBW', 'lina',2);
INSERT INTO rols(id, description) VALUES (1,'ROLE_ADMIN'), (2,'ROLE_USER'), (3,'ROLE_MANAGER');
INSERT INTO user_has_rol(id_user, id_rol) VALUES (1,1), (1,2),(2,1),(2,3);
