package com.adsi.bike.domain.enumeration;

public enum Gender {
    MALE, FEMALE, OTHER
}
