package com.adsi.bike.domain;


import com.adsi.bike.domain.enumeration.Gender;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DetailUser {

    @Id@GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private long id;

    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    @NotNull
    @Size(max = 20, min =4)
    @Column(name = "document_number", length = 20)
    private String documentNumber;

    @Enumerated(EnumType.STRING)
    private Gender gender;
}
