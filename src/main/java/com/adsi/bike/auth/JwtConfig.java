package com.adsi.bike.auth;

public class JwtConfig {

    public static final String RSA_PRIVATE = "-----BEGIN RSA PRIVATE KEY-----\n" +
            "MIIEpAIBAAKCAQEA3H6ejH7QznL+l2uTfEBO58u6jeYhTcIlitiOHOGlMsccTRfV\n" +
            "daH8TucvxGQ7jOcK3+KXShXLE40Qi+2HP0Eh4w9IQSEKfuqVejZhzDsxM40ZPWbY\n" +
            "/McpJ3FtE1rT9d1YD/TCG31FZrwq3aUd5weIm1SQ2hkNOe8NjGv+dqaBHCRVGYnr\n" +
            "wYd70OqCltxBUMhBh4NhsT//AgSyIgpTfiYYgZ4NUFibOAn4yhn1MFWupjYq7Tnz\n" +
            "fGfo9FWluTMjoAj1jJEeDmWb0OPEth8XIHm9IbZVEydewtPKYEkAP0cKrAOBTeis\n" +
            "m2Mef2zHR/cVRkBZ+TlS+Po1V8qAKT8Lki7shQIDAQABAoIBAQDJwFnWeBsbLAFk\n" +
            "D5zNrhIE6lbyi8WhXqn3V5sBNj5wLdvpdotsrSEuGT+N1aDflnBO1whTU4mThBnF\n" +
            "zztXV4NoPY2BengeFQtc3fGya97jKpITqs2Uza8MVi7V7N/RMgzn3K7XdFJSZI5c\n" +
            "bD7mK/CFIGXCCfYeMmwVNqOJcWTll241cOxgc1MAZ3b/rIxulKHAqVJ0xZ5/uM79\n" +
            "Tqp8urMu1LwfaCd9HvTEUD3jGVcDqjDeZbv+K7hhx+TPfBgLNdi94nZPe+20bgFz\n" +
            "7o5TvvyxjBWspN7U8wYucblGQOQKBhFFFhH07TuIF9pXYHMwVHEUAvYIl7flLGzV\n" +
            "zxzG/8tpAoGBAO4S3C5LHOWmqRauBRWB4DMqAcBMlXLUTfLs4EU3e0IAg37MieTK\n" +
            "TWiEI23DZpXqlxrVr1FLgDS5BEAhSi2KGaWasDpMltE4hEsEvuLHlloXGlj1CcB+\n" +
            "O4ub3HQPF3o1HvRCIDQ2rKhyeHSX6JoBG34UPNujnzS+OipBVfx0AOZvAoGBAO0Y\n" +
            "5yA8HEbdGkbMOyTlYgfwZHnyTAzaBGGniNjwz5qIwRkj2ITm3Z+0c4BZpkhYksqN\n" +
            "+NEmwSJ+7AmmAt47pfyWexI2ptGyiy7JqrdLvgwuKt/cmZg/0o5aldKEagYgBDR5\n" +
            "gov3mS6JqmFtinQPgA7FgXts0/Xl0R1ZGeUXsDZLAoGActuCgosWe+018o0uQrwP\n" +
            "8F85zQ6lo2qt2J9Ta9Fik+svhYMkm87GHcByX0hAO9+b2wRwuqXUSptgY5gr82CH\n" +
            "ExBYKSg+Mt4zZQX7Gen0ra4rtfMyKz47eBVP/GYzi3AJaQpmAwrFrdbcKlQHkhUq\n" +
            "sF8PJWABNS75XN18oHlO3d0CgYEAhEl+u5eOV2OKA7Jz1XQ+rBtOlFPCZvFFvI7T\n" +
            "ZeeGjbx7sOE1L4H4aeEvCI/8nST+UhHAoEG1RN/4JrH/q6swQ6xaYqEnlpy9148h\n" +
            "tz+FfBpORbU55Z7GhjK4oUrMNTqr3fNmCV5Ok46wB8gslfEIDDXattjTssFVWcCb\n" +
            "GB2Nbd0CgYBJLYnJFelLPQ5V3U1aGhzRPAIa2Y8ENx56plphBrqHdxJPasC1O1KA\n" +
            "hde1JrIdk2JJGy+m0D2DUtSZn/V1spFfmEPfzRn3brT304VHS3/nvKGng66P0rht\n" +
            "RzYYztxYK1gLgglWs0dRG5EL+n3dYkCldP4SFi3O2/o41jL1bT4WnQ==\n" +
            "-----END RSA PRIVATE KEY-----";

    public static final String RSA_PUBLIC = "-----BEGIN PUBLIC KEY-----\n" +
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA3H6ejH7QznL+l2uTfEBO\n" +
            "58u6jeYhTcIlitiOHOGlMsccTRfVdaH8TucvxGQ7jOcK3+KXShXLE40Qi+2HP0Eh\n" +
            "4w9IQSEKfuqVejZhzDsxM40ZPWbY/McpJ3FtE1rT9d1YD/TCG31FZrwq3aUd5weI\n" +
            "m1SQ2hkNOe8NjGv+dqaBHCRVGYnrwYd70OqCltxBUMhBh4NhsT//AgSyIgpTfiYY\n" +
            "gZ4NUFibOAn4yhn1MFWupjYq7TnzfGfo9FWluTMjoAj1jJEeDmWb0OPEth8XIHm9\n" +
            "IbZVEydewtPKYEkAP0cKrAOBTeism2Mef2zHR/cVRkBZ+TlS+Po1V8qAKT8Lki7s\n" +
            "hQIDAQAB\n" +
            "-----END PUBLIC KEY-----";
}
