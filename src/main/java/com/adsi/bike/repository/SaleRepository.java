package com.adsi.bike.repository;

import com.adsi.bike.domain.Sale;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

public interface SaleRepository extends CrudRepository<Sale, Integer> {

    //query find all by model, email and document number
    Page<Sale> findAllByBike_ModelAndClient_EmailAndClient_DocumentNumber(String model, String email, String documentNumber, Pageable pageable);
    Page<Sale> findAllByBike_ModelAndClient_Email(String model, String email, Pageable pageable);
    Page<Sale> findAllByBike_ModelAndClient_DocumentNumber(String model,  String documentNumber, Pageable pageable);
    Page<Sale> findAllByClient_EmailAndClient_DocumentNumber( String email, String documentNumber, Pageable pageable);
}
