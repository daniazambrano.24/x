package com.adsi.bike.service.transformer;

import com.adsi.bike.domain.Users;
import com.adsi.bike.service.dto.UserDTO;

public class UserTransformer {
    public static UserDTO getUserDTOFromUser(Users users){
        if(users == null){
            return null;
        }
        UserDTO dto = new UserDTO();

        dto.setId(users.getId());
        dto.setEmail(users.getEmail());
        dto.setEnabled(users.getEnabled());
        dto.setUsername(users.getUsername());
        dto.setRols(users.getRols());
        return dto;
    }
}
