package com.adsi.bike.service;

import com.adsi.bike.domain.Rols;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface IRolsService {

    public Iterable<Rols> read();

    public Optional<Rols> getById(Integer id);

    public ResponseEntity create(Rols rols);

    public Rols update(Rols rols);


}
