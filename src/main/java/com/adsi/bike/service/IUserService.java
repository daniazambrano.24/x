package com.adsi.bike.service;

import com.adsi.bike.domain.Users;
import com.adsi.bike.service.dto.DataUserDTO;
import com.adsi.bike.service.dto.UserDTO;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface IUserService {


    public Iterable<Users> read();

    public Optional<Users> getById(Long id);

    public ResponseEntity create(Users users);

    public Users update(Users users);

    public DataUserDTO getDataUser(String documentNumber);

    UserDTO getByUsername(String username);

}
