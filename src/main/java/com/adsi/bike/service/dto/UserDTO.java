package com.adsi.bike.service.dto;

import com.adsi.bike.domain.Rols;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
public class UserDTO {

    @Id
    private long id;

    @NotNull
    @Size(max=20, min=5)
    private String username;

    @NotEmpty(message = "el campo no puede estar vacio")
    @Email(message = "El email no tiene la estructura corecta ")
    private String email;

    private Boolean enabled;

    private List<Rols> rols;

    private List<String> authorities;

}
