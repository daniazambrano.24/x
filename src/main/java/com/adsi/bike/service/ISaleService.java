package com.adsi.bike.service;

import com.adsi.bike.domain.Sale;
import com.adsi.bike.service.dto.SaleFilterDTO;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface ISaleService {

    public Iterable<Sale> read();

    public Optional<Sale> getById(Integer id);

    public Sale create (Sale sale);

    public Sale update (Sale sale);

    public void delete (Integer id);

    public Sale createList(List<Sale> sale);

    public Page<SaleFilterDTO> getFilter(Integer pageSize,
                                         Integer pageNumber,
                                         String model,
                                         String documentNumber,
                                         String email);
}
