package com.adsi.bike.web.rest;

import com.adsi.bike.domain.Users;
import com.adsi.bike.service.IUserService;
import com.adsi.bike.service.dto.DataUserDTO;
import com.adsi.bike.service.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class UserResoruce {


    @Autowired
    IUserService userService;

    @PostMapping("/user")
    public ResponseEntity create(@RequestBody Users user){
        return userService.create(user);
    }
    @GetMapping("/user")
    public Iterable<Users> read(){
        return userService.read();
    }

    @GetMapping("/user/{documentNumber}")
    public DataUserDTO getDataUser(@PathVariable String documentNumber){
        return userService.getDataUser(documentNumber);
    }
    @GetMapping("/user/account")
    public ResponseEntity<UserDTO> getAccount(@RequestParam(value = "username") String username){
        return  new ResponseEntity<>(userService.getByUsername(username), HttpStatus.OK);
    }
}
