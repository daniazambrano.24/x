package com.adsi.bike.web.rest;

import com.adsi.bike.domain.Rols;
import com.adsi.bike.service.IRolsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class RolsResource {

    @Autowired
    IRolsService rolsService;

    @PostMapping("/rol")
    public ResponseEntity create (@RequestBody Rols rol){
        return rolsService.create(rol);
    }
    @GetMapping("/rol")
    public Iterable<Rols> read(){
        return rolsService.read();
    }
}
